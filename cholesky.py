## module cholesky

from numpy import dot, outer, transpose
from math import sqrt

def decompose(A):
    flag = True
    msg  = ""
    n    = len(A)

    for k in range(n):
        # update a_11
        try:
            A[k,k] = sqrt(A[k,k])
        except:
            flag = False
            msg  = "Matrix is not positive definite"
            return A, flag, msg

        # update s
        A[k,k+1:n] = A[k,k+1:n] / A[k,k]
        A[k+1:n,k] = A[k+1:n,k] / A[k,k]

        # update A^ with outer product of s and s'
        A[k+1:n,k+1:n] = A[k+1:n,k+1:n] - outer(A[k,k+1:n], A[k+1:n,k])
    for k in range(1, n):
        A[0:k,k] = 0.0

    return A, flag, msg
