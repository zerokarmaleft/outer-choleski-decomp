#!/usr/bin/env python

from numpy import *
from numpy.linalg import *
import cholesky
import sys

if __name__ == "__main__":
    txtFile = "pp4DataA1.txt"
    if len(sys.argv) > 1:
        txtFile = sys.argv[1]
        print "Using data file " + txtFile + " for matrix A."

    # check for valid text files
    try:
        A = asarray(loadtxt(txtFile, dtype=float))
    except Exception as e:
        print "Failed to load matrix A data file, " + txtFile + "."
        print "Exception : " + e
        sys.exit(-1)

    # find Cholesky's decomposition
    L, flag, msg = cholesky.decompose(A.copy())
    if not flag:
        print "Failed to factor matrix A."
        print "Error message : " + msg
        print "Initial A = \n" + A
        sys.exit(-2)

    # display Cholesky's decomposition
    print "Cholesky's decomposition of A : \n" + array_str(A) + " = \n" + array_str(L) + " * \n" + array_str(transpose(L))
