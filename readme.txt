Edward Cho
Dr. Doug Heisterkamp
CS 3513, Section 801
26, April 2012

Programming Problem #4
Outer Product Choleski Decomposition
===================================

The 'choleski' module implements Choleski's decomposition iteratively
using the following properties:

[[a_11, bT],   [[r_11, 0  ], [[r_11, sT],
 [b,    A^]] =  [s,    G^T]]  [0,    G^]

a_11 = {r_11}^2
bT   = r_11 * sT
A^   = s * sT + R^T * R^

Running
===================================
$ python pp4.py <input-file>
